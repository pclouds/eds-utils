#define HANDLE_LIBICAL_MEMORY
#include <libecal/e-cal.h>
#include <string.h>

static gchar *itip_methods[] = {
	"PUBLISH",
	"REQUEST",
	"REPLY",
	"ADD",
	"CANCEL",
	"RERESH",
	"COUNTER",
	"DECLINECOUNTER"
};

static icalproperty_method itip_methods_enum[] = {
    ICAL_METHOD_PUBLISH,
    ICAL_METHOD_REQUEST,
    ICAL_METHOD_REPLY,
    ICAL_METHOD_ADD,
    ICAL_METHOD_CANCEL,
    ICAL_METHOD_REFRESH,
    ICAL_METHOD_COUNTER,
    ICAL_METHOD_DECLINECOUNTER,
};

static GMainLoop *main_loop;
static char *option_format;
static char *option_method;
static char *option_timezone;
static char *option_source;
static int option_update;
static GOptionEntry common_options[] = {
	{ "format", 'f', 0, G_OPTION_ARG_STRING, &option_format, "Output format", "format" },
	{ "method", 'm', 0, G_OPTION_ARG_STRING, &option_method, "Itip method", "method" },
	{ "timezone", 'z', 0, G_OPTION_ARG_STRING, &option_timezone, "Timezone", "timezone" },
	{ "source", 's', 0, G_OPTION_ARG_STRING, &option_source, "iCal source", "path" },
	{ "update", 's', 0, G_OPTION_ARG_NONE, &option_update, "update existing item", NULL },
	{ NULL }
};

static void print_icalcomponent (icalcomponent *comp, const char *format);

static icalproperty_method
get_method (const char *name)
{
	int i;
	for (i = 0; i < G_N_ELEMENTS (itip_methods); i ++)
		if (!strcasecmp(itip_methods[i], name))
			return itip_methods_enum[i];
	g_print ("Invalid Itip method %s\n", name);
	exit (-1);
}

static int
print_vcalendar (icalcomponent *comp)
{
	icalcomponent *top_level;
	icalproperty *prop;
	icalvalue *value;

	top_level = icalcomponent_new (ICAL_VCALENDAR_COMPONENT);

	/* RFC 2445, section 4.7.1 */
	prop = icalproperty_new_calscale ("GREGORIAN");
	icalcomponent_add_property (top_level, prop);

	/* RFC 2445, section 4.7.3 */
	prop = icalproperty_new_prodid ("-//Ximian//NONSGML Evolution Calendar//EN");
	icalcomponent_add_property (top_level, prop);

	/* RFC 2445, section 4.7.4.  This is the iCalendar spec version, *NOT*
	 * the product version!  Do not change this!
	 */
	prop = icalproperty_new_version ("2.0");
	icalcomponent_add_property (top_level, prop);

	if (option_method) {
		prop = icalproperty_new (ICAL_METHOD_PROPERTY);
		value = icalvalue_new_method (get_method (option_method));
		icalproperty_set_value (prop, value);
		icalcomponent_add_property (top_level, prop);
	}

	/* Example: /softwarestudio.org/Olson_20011030_5/Asia/Saigon */
	if (option_timezone) {
		icaltimezone *zone;
		icalcomponent *vtimezone_comp;

		zone = icaltimezone_get_builtin_timezone_from_tzid (option_timezone);
		vtimezone_comp = icaltimezone_get_component (zone);
		if (!vtimezone_comp) {
			g_print ("Unable to get timezone %s\n", option_timezone);
			return -1;
		}

		icalcomponent_add_component (top_level, icalcomponent_new_clone (vtimezone_comp));
	}

	icalcomponent_add_component (top_level, icalcomponent_new_clone (comp));
	printf ("%s", icalcomponent_as_ical_string (top_level));
	icalcomponent_free (top_level);
	return 0;
}

static int
print_ical_string (icalcomponent *comp)
{
	printf ("%s", icalcomponent_as_ical_string (comp));
	return 0;
}

static int
print_calendar (icalcomponent *comp)
{
	print_icalcomponent (comp,
			     "(%(type)) %(summary)\n"
			     "From: %(dtstart)\n"
			     "To: %(dtend)\n"
			     "Location: %(location)\n"
			     "\n"
			     "%(description)\n");
	return 0;
}

static int
print_summary (icalcomponent *comp)
{
	printf ("%s", icalcomponent_get_summary (comp));
	return 0;
}

static int
print_comment (icalcomponent *comp)
{
	printf ("%s", icalcomponent_get_comment (comp));
	return 0;
}

static int
print_description (icalcomponent *comp)
{
	printf ("%s", icalcomponent_get_description (comp));
	return 0;
}

static int
print_location (icalcomponent *comp)
{
	printf ("%s", icalcomponent_get_location (comp));
	return 0;
}

static int
print_uid (icalcomponent *comp)
{
	printf ("%s", icalcomponent_get_uid (comp));
	return 0;
}

static int
print_status (icalcomponent *comp)
{
	printf ("%s", icalproperty_status_to_string (icalcomponent_get_status (comp)));
	return 0;
}

static int
print_type (icalcomponent *comp)
{
	printf ("%s", icalcomponent_kind_to_string (icalcomponent_isa (comp)));
	return 0;
}

static void
print_time (const icaltimetype t)
{
	time_t t2 = icaltime_as_timet (t);
	struct tm *tm = localtime (&t2);
	char buf[200];
	strftime (buf, 200, "%a, %d %b %Y %H:%M:%S %z", tm);
	printf ("%s", buf);
}

static int
print_dtstart (icalcomponent *comp)
{
	print_time (icalcomponent_get_dtstart (comp));
	return 0;
}

static int
print_dtend (icalcomponent *comp)
{
	print_time (icalcomponent_get_dtend (comp));
	return 0;
}

static void
print_icalcomponent (icalcomponent *comp, const char *format)
{
	const char *s = format;
	char c;
	int i, ret;
	struct {
		char short_option;
		const char *long_option;
		int (*handler)(icalcomponent *comp);
	} format_types[] = {
		{ 'v', "ical", print_ical_string },
		{ 'V', "vcalendar", print_vcalendar },
		{ 'h', "calendar", print_calendar },
		{ 't', "type", print_type },
		{ 'S', "status", print_status },
		{ 'u', "uid", print_uid },
		{ 'l', "location", print_location },
		{ 'd', "description", print_description },
		{ 'c', "comment", print_comment },
		{ 's', "summary", print_summary },
		{ '\0', "dtstart", print_dtstart },
		{ '\0', "dtend", print_dtend },
	};

	if (!s)
		s = "%u\n";

	while ((c = *s++) != '\0') {
		switch (c) {
		case '%':
			c = *s++;
			if (c == '(') {
				const char *start = s;
				const char *end;
				int len;
				while (c != '\0' && c != ')')
					c = *s++;
				if (c == '\0') {
					g_print ("Invalid format string: %s\n", format);
					exit (-1);
				}
				len = (s - 1) - start;
				for (i = 0;i < G_N_ELEMENTS (format_types);i ++)
					if (format_types[i].long_option &&
					    strlen (format_types[i].long_option) == len &&
					    !strncmp (format_types[i].long_option, start, len))
						break;
				if (i == G_N_ELEMENTS (format_types)) {
					char *str = malloc (len+1);
					memcpy (str, start, len);
					str[len] = '\0';
					g_print ("Unknown format type %s\n", str);
					free (str);
					exit (-1);
				}
			}
			else {
				for (i = 0;i < G_N_ELEMENTS (format_types);i ++)
					if (format_types[i].short_option &&
					    c == format_types[i].short_option)
						break;
				if (i == G_N_ELEMENTS (format_types)) {
					g_print ("Unknown format type '%c'\n", (int)c);
					exit (-1);
				}
			}
			ret = format_types[i].handler (comp);
			if (ret)
				exit (ret);
			break;
		case '\\':
			c = *s++;
			switch (c) {
			case 'n': printf ("\n"); break;
			case 'r': printf ("\r"); break;
			case 't': printf ("\t"); break;
			default: printf ("%c", *s);
			}
			break;
		default:
			printf ("%c", c);
		}
	}
}

static void
cal_opened_for_query_cb (ECal *ecal, gint arg1, const char *udata)
{
	GList *lst, *head;
	GError *err = NULL;
	gboolean ret;
	ret = e_cal_get_object_list(ecal, udata, &head, &err);
	if (!ret) {
		g_print("Unable to query object list\n");
		exit(-1);
	}
	lst = head;
	while (lst) {
		icalcomponent *comp = (icalcomponent*)lst->data;
		print_icalcomponent (comp, option_format);
		lst = lst->next;
	}
	e_cal_free_object_list(head);
	g_main_loop_quit(main_loop);
}

static int
cmd_query (int argc, char **argv)
{
	ECal *ecal;

	if (argc < 3) {
		g_print ("Query required\n");
		return -1;
	}
	ecal = e_cal_new_system_calendar ();
	g_signal_connect(G_OBJECT(ecal), "cal_opened", G_CALLBACK(cal_opened_for_query_cb), argv[2]);
	e_cal_open_async(ecal, TRUE);
	main_loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(main_loop);
	return 0;
}

static int
cmd_cat (int argc, char **argv)
{
	gchar *contents;
	gsize length;
	GError *error = NULL;
	icalcomponent *comp;

	if (!option_source) {
		g_print ("--source required\n");
		return -1;
	}

	if (!option_format)
		option_format = "%h\n";

	if (!g_file_get_contents (option_source, &contents, &length, &error)) {
		g_print ("Error opening file %s\n", option_source);
		return -1;
	}
	comp = icalparser_parse_string (contents);
	if (!comp) {
		g_print ("%s is not a valid iCalendar\n", option_source);
		return -1;
	}
	print_icalcomponent (comp, option_format);
	return 0;
}

static void
cal_opened_for_import_cb (ECal *ecal, gint arg1, const char *udata)
{
	icalcompiter iter;
	icalcomponent *ical_comp;
	icalcomponent_kind kind;
	GError *error = NULL;
	gboolean ret;
	char *uid;

	iter = icalcomponent_begin_component ((icalcomponent *)udata, ICAL_ANY_COMPONENT);
        while (1) {
		ical_comp = icalcompiter_deref (&iter);
                if (!ical_comp)
                        break;
		kind = icalcomponent_isa (ical_comp);
		if (kind != ICAL_VEVENT_COMPONENT
		    && kind != ICAL_VTODO_COMPONENT
		    && kind != ICAL_VFREEBUSY_COMPONENT
		    && kind != ICAL_VJOURNAL_COMPONENT)
                        goto next;

                uid = NULL;
                if (option_update)
                        ret = e_cal_modify_object (ecal, ical_comp, CALOBJ_MOD_THIS, &error);
                else
                        ret = e_cal_create_object (ecal, ical_comp, &uid, &error);
                if (!ret) {
                        g_print ("Unable to import object\n");
                        exit (-1);
                }
                if (!option_update && uid)
                        g_print ("New object created with UID %s\n", uid);

next:
                icalcompiter_next (&iter);
        }
	g_main_loop_quit (main_loop);
}

static int
cmd_import (int argc, char **argv)
{
	ECal *ecal;
	gchar *contents;
	gsize length;
	GError *error = NULL;
	icalcomponent *comp;

	if (argc < 3) {
		g_print ("iCalendar file required\n");
		return -1;
	}
	if (!g_file_get_contents (argv[2], &contents, &length, &error)) {
		g_print ("Error opening file %s\n", argv[2]);
		return -1;
	}
	comp = icalparser_parse_string (contents);
	if (!comp) {
		g_print ("%s is not a valid iCalendar\n", argv[2]);
		return -1;
	}
	ecal = e_cal_new_system_calendar ();
	g_signal_connect(G_OBJECT(ecal), "cal_opened", G_CALLBACK(cal_opened_for_import_cb), comp);
	e_cal_open_async(ecal, TRUE);
	main_loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(main_loop);
	return 0;
}

static void
cal_opened_for_rm_cb (ECal *ecal, gint arg1, const char *udata)
{
	GError *error = NULL;
	gboolean ret;
	if (!e_cal_remove_object (ecal, (const char *)udata, &error)) {
		g_print("Unable to query object list\n");
		exit(-1);
	}
	g_main_loop_quit(main_loop);
}

static int
cmd_rm (int argc, char **argv)
{
	ECal *ecal;

	if (argc < 3) {
		g_print ("Uid required\n");
		return -1;
	}
	ecal = e_cal_new_system_calendar ();
	g_signal_connect(G_OBJECT(ecal), "cal_opened", G_CALLBACK(cal_opened_for_rm_cb), argv[2]);
	e_cal_open_async(ecal, TRUE);
	main_loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(main_loop);
	return 0;
}

struct cmd {
	const char *name;
	int (*handler)(int argc, char **argv);
	const char *desc;
} cmds[] = {
	{ "query", cmd_query, "Query an item from system calendar" },
	{ "cat", cmd_cat, "Show a local iCalendar file" },
	{ "import", cmd_import, "Add/Update an item to system calendar from file" },
	{ "rm", cmd_rm, "Remove an item from system calendar by UID" },
};

int main(int argc, char **argv)
{
	GOptionContext *opt;
	GError *error = NULL;
	int i;

	g_type_init();

	if (argc < 2) {
		g_print("Require a subcommand. Supported commands are:\n");
		for (i = 0;i < G_N_ELEMENTS (cmds);i ++)
			g_print ("\t%s\t%s\n", cmds[i].name, cmds[i].desc);
		g_print ("\n");
		return -1;
	}
	for (i = 0;i < G_N_ELEMENTS (cmds);i ++)
		if (!strcmp (argv[1], cmds[i].name))
			break;
	if (i == G_N_ELEMENTS (cmds)) {
		g_print("Unsupported command %s\n", argv[1]);
		return -1;
	}
	opt = g_option_context_new ("subcommand [options]");
	g_option_context_add_main_entries (opt, common_options, "ecal");
	if (!g_option_context_parse (opt, &argc, &argv, &error)) {
		g_print ("option parsing failed: %s\n", error->message);
		exit (1);
	}
	return cmds[i].handler (argc, argv);
}
